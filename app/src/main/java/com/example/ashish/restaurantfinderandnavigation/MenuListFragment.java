package com.example.ashish.restaurantfinderandnavigation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.ashish.restaurantfinderandnavigation.fragments.MyAccount;


/**
 * Created by Srijana on 3/23/2017.
 */

public class MenuListFragment extends Fragment {
    private ImageView user_profile_pic;
    private FragmentActivity myContext;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        user_profile_pic = (ImageView) view.findViewById(R.id.profile_pic);
        NavigationView vNavigation = (NavigationView) view.findViewById(R.id.vNavigation);
        vNavigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                FragmentManager fm= getFragmentManager();
                fm.beginTransaction().add(R.id.id_container_menu, new MyAccount()).commit();
//                selectDrawerItem(menuItem);
                Toast.makeText(getActivity(), menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                return false;
            }
        });




        return view;
    }

    private void selectDrawerItem(MenuItem menuItem) {
        Fragment fragment = null;
//        Class FragmentClass;
         switch(menuItem.getItemId()){

             default:
           fragment=new MyAccount();
                 break;

         }

         FragmentManager fm = getFragmentManager();
        fm.beginTransaction().add(R.id.id_container_menu, fragment).commit();

    }
}
