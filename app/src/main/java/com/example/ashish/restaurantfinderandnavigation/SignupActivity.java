package com.example.ashish.restaurantfinderandnavigation;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ashish on 24/03/2017.
 */

public class SignupActivity extends AppCompatActivity {

    SharedPreferences pref;

    EditText userName, emailAddress, passWord, confirmPassword;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        final Button register_button = (Button) findViewById(R.id.register_button);

        userName = (EditText) findViewById(R.id.username);
        emailAddress = (EditText) findViewById(R.id.email_address);
        passWord = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);

        final TextView username_error = (TextView) findViewById(R.id.error_username);
        final TextView email_address_error = (TextView) findViewById(R.id.error_email_address);
        final TextView password_error = (TextView) findViewById(R.id.error_password);
        final TextView confirm_password_error = (TextView) findViewById(R.id.error_confirm_password);
        final CheckBox checkBox = (CheckBox) findViewById(R.id.checkbox);

        pref = getSharedPreferences("log", Context.MODE_PRIVATE);

        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean isValid = true;
                String uName = userName.getText().toString();
                String eMail = emailAddress.getText().toString();
                String pWord = passWord.getText().toString();
                String con_pWord = confirmPassword.getText().toString();

                //For Username
                if (uName.equals("")) {
                    username_error.setVisibility(View.VISIBLE);
                    isValid = false;
                } else {
                    username_error.setVisibility(View.GONE);
                }

                //For Email Address
                if (TextUtils.isEmpty(eMail)) {
                    email_address_error.setVisibility(View.VISIBLE);
                    isValid = false;
                } else {
                    if (Patterns.EMAIL_ADDRESS.matcher(eMail).matches()) {
                        email_address_error.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(SignupActivity.this, "Invalid Email Address", Toast.LENGTH_SHORT).show();
                    }
                }

                //For Password
                if (pWord.equals("")) {
                    password_error.setVisibility(View.VISIBLE);
                    isValid = false;
                } else {
                    if (pWord.length() > 6) {
                        if (pWord.equals(con_pWord)) {
                            password_error.setVisibility(View.GONE);
                        } else {
                            password_error.setText("Password do not match");
                            password_error.setVisibility(View.VISIBLE);
                            isValid = false;
                        }
                    } else {
                        password_error.setText("Password should have more than six characters.");
                        password_error.setVisibility(View.VISIBLE);
                        isValid = false;
                    }
                }

                //For CheckBox

                if (!checkBox.isChecked()) {
                    Toast.makeText(SignupActivity.this, "Please, agree terms and conditions", Toast.LENGTH_SHORT).show();
                    isValid = false;
                } else {

                }

                //isValid()

                if (isValid) {
//                    Toast.makeText(SignupActivity.this, "Successfully registered", Toast.LENGTH_SHORT).show();
//                    SharedPreferences.Editor editor = pref.edit();
//
//                    editor.putString("Username:- ", uName);
//                    editor.putString("E-mail Address:- ", eMail);
//                    editor.putString("Password:- ", pWord);
//                    editor.putString("Confirmed Password:- ", con_pWord);
//
//                    editor.commit();

                    signuptask(uName, eMail, pWord, con_pWord);
//
//                    pref.edit().clear().commit();
//                    Intent intent_home = new Intent(getApplicationContext(),HomeActivity.class);
//                    intent_home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent_home);

                } else {
                    Toast.makeText(SignupActivity.this, "Error", Toast.LENGTH_SHORT).show();

                }

            }

        });

    }

    private void signuptask(final String username, final String email, final String password, final String confirmpassword) {
        String url = ServerConnection.myserver1 + "registerprocess.php";

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setIcon(R.drawable.temp_app_logo);
        progressDialog.show();

        StringRequest req = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", response);
                progressDialog.dismiss();

                try {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SignupActivity.this);
                    builder.setTitle("Successfully Registered!!");
                    builder.setPositiveButton("Login Now", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        }
                    });
                    builder.setIcon(R.drawable.temp_app_logo);
                    builder.show();
                } catch (Exception e) {
                    Toast.makeText(SignupActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                try {
                    JSONArray errors = new JSONArray(error);
                } catch (JSONException e) {
                    Log.e("mesg", error.getMessage() + "");
                    e.printStackTrace();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> postparameter = new HashMap<>();
                postparameter.put("username", username);
                postparameter.put("email", email);
                postparameter.put("password", password);
                postparameter.put("confirmpassword", confirmpassword);
                return postparameter;

            }
        };

        Volley.newRequestQueue(getApplicationContext()).add(req);


    }
}
