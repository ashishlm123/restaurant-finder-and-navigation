package com.example.ashish.restaurantfinderandnavigation.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ashish.restaurantfinderandnavigation.InsideSpecificMenuActivity;
import com.example.ashish.restaurantfinderandnavigation.NearbyActivity;
import com.example.ashish.restaurantfinderandnavigation.R;

/**
 * Created by Ashish on 21/04/2017.
 */

public class MyHome extends Fragment {
    String[] menuTitle = new String[]{"RESTAURANT", "HOTEL", "RESORT", "CAFE AND COFFEE HOUSE", "PUB AND BAR",
            "NIGHT CLUB", "LOUNGE AND LIVE MUSIC"};
//    int[] menuIcon = new int[]{R.drawable.restaurant, R.drawable.cafe, R.drawable.pub_and_bars,
//            R.drawable.dining, R.drawable.longue, R.drawable.night_clubs, R.drawable.livemusic};

    int[] menuBackground = {R.drawable.restaurant_background,
            R.drawable.hotels_background,
            R.drawable.resort_background,
            R.drawable.coffee_house_background,
            R.drawable.pub_and_bars_background,
            R.drawable.night_clubs_background,
            R.drawable.loungue_and_live_music_background};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_home, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            View view1 = LayoutInflater.from(getActivity()).inflate(R.layout.cutsom_dialog_location,null, false);
            alertDialogBuilder.setView(view1);
            alertDialogBuilder.setPositiveButton("Turn Gps On", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    intentToGpsSettings();
//                    RecyclerView homeMenuList = (RecyclerView) view.findViewById(R.id.home_menu);
//                    homeMenuList.setLayoutManager(new LinearLayoutManager(getActivity()));
//                    homeMenuList.setAdapter(new HomeMenuAdapter());
                }
            });

            alertDialogBuilder.setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });

            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.show();
        }
        else{
            Toast.makeText(getActivity(), "Welcome Home", Toast.LENGTH_SHORT).show();
        }
        RecyclerView homeMenuList = (RecyclerView) view.findViewById(R.id.home_menu);
        homeMenuList.setLayoutManager(new LinearLayoutManager(getActivity()));
        homeMenuList.setAdapter(new HomeMenuAdapter());
    }

    //Takes user to location setting.
    public void intentToGpsSettings()
    {
        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }

    private class HomeMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(getActivity()).inflate(
                    R.layout.home_menu, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ViewHolder viewHolder = (ViewHolder) holder;
//            viewHolder.mImg.setImageResource(menuIcon[position]);
            viewHolder.mTitle.setText(menuTitle[position]);
            viewHolder.mBackground.setBackgroundResource(menuBackground[position]);
        }

        @Override
        public int getItemCount() {
            return menuTitle.length;
        }

        //Creation of Vieholder to hold the recyclerView.
        private class ViewHolder extends RecyclerView.ViewHolder {
            TextView mTitle;
            RelativeLayout mBackground;

            ViewHolder(View itemView) {
                super(itemView);
                mTitle = itemView.findViewById(R.id.menu_title);
                mBackground = itemView.findViewById(R.id.mBackground);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (checkInternetConnection()) {
                            Toast.makeText(getActivity(), menuTitle[getLayoutPosition()], Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), NearbyActivity.class);
                            intent.putExtra("place", menuTitle[getLayoutPosition()]);
                            intent.putExtra("Title", menuTitle[getLayoutPosition()]);
                            intent.putExtra("image", menuBackground[getLayoutPosition()]);
                            getActivity().startActivity(intent);
                        } else {
                            Toast.makeText(getActivity(), "Please, check your internet connection...", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }

        private boolean checkInternetConnection() {
            ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            // ARE WE CONNECTED TO THE NET
            if (conMgr.getActiveNetworkInfo() != null
                    && conMgr.getActiveNetworkInfo().isAvailable()
                    && conMgr.getActiveNetworkInfo().isConnected()) {
                return true;
            }
            return false;
        }


    }
}



