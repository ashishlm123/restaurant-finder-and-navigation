package com.example.ashish.restaurantfinderandnavigation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by Ashish on 18/04/2017.
 */

public class ResetPasswordActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        Button return_to_login = (Button) findViewById(R.id.return_to_login);
        return_to_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_login_page = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent_login_page);
            }
        });

    }
}
