package com.example.ashish.restaurantfinderandnavigation.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ashish.restaurantfinderandnavigation.R;
import com.example.ashish.restaurantfinderandnavigation.RestaurantDetailsActivity;
import com.example.ashish.restaurantfinderandnavigation.helper.TypeModel;

import java.util.ArrayList;

/**
 * Created by Ashish on 12/05/2017.
 */

    public class TypeModelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<TypeModel> typeModelArrayList;
    private Context context;

    int image;
    String Title;

    //
    public void setImageResource(int image){
        this.image=image;
    }




    public TypeModelAdapter(ArrayList<TypeModel> typeModelArrayList, Context Context) {
        this.typeModelArrayList = typeModelArrayList;
        this.context=Context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.inside_specific_menu_list ,parent,false));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder=(ViewHolder)holder;
        TypeModel typeModel=typeModelArrayList.get(position);
        viewHolder.textView1.setText(typeModel.getName());
        viewHolder.textView2.setText(typeModel.getLocation());
        viewHolder.textView3.setText(typeModel.getRating());
    }

    @Override
    public int getItemCount() {
        Log.e("arraysize", String.valueOf(typeModelArrayList.size()));
        return typeModelArrayList.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{
        TextView textView1, textView2, textView3;

        public ViewHolder(View itemView) {
            super(itemView);
            textView1= (TextView)itemView.findViewById(R.id.nearby_place_title);
            textView2 = (TextView) itemView.findViewById(R.id.nearby_place_location);
            textView3 = (TextView) itemView.findViewById(R.id.nearby_place_rating);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, RestaurantDetailsActivity.class);
                    intent.putExtra("image",image);
                    intent.putExtra("type",typeModelArrayList.get(getLayoutPosition()));
                    context.startActivity(intent);

                }
            });
        }
    }
}



