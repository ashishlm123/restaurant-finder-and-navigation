package com.example.ashish.restaurantfinderandnavigation;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ashish.restaurantfinderandnavigation.adapter.PlaceModelAdapter;
import com.example.ashish.restaurantfinderandnavigation.adapter.TypeModelAdapter;
import com.example.ashish.restaurantfinderandnavigation.helper.DownloadUrl;
import com.example.ashish.restaurantfinderandnavigation.helper.PlaceModel;
import com.example.ashish.restaurantfinderandnavigation.helper.TypeModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ashish on 24/04/2017.
 */

public class InsideSpecificMenuActivity extends AppCompatActivity {
    private int PROXIMITY_RADIUS = 3000;
    double latitude;
    double longitude;
    LocationManager lm;
    ArrayList<PlaceModel> placeModelArrayList;
    PlaceModelAdapter placeModelAdapter;
    String nearbyPlace;
    String Title;
    ArrayList<TypeModel> typeModelArrayList;
    int image;
    ProgressBar progressBar;
    ProgressDialog progressDialog;
    RecyclerView specificMenuList;
    TextView pleaseWait;
    ImageView imageView;
    FloatingActionButton floatingActionButton;
    TypeModelAdapter typeModelAdapter;
    Toolbar toolbar;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inside_specific_menu);

        floatingActionButton = (FloatingActionButton) findViewById(R.id.nearby_places);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), NearbyActivity.class);
                intent.putExtra("Title", Title);
                startActivity(intent);
            }
        });


        imageView = (ImageView) findViewById(R.id.specificCoverImage);

        typeModelArrayList = new ArrayList<>();
        placeModelArrayList = new ArrayList<>();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(0xFFFF0000, android.graphics.PorterDuff.Mode.MULTIPLY);
        pleaseWait = (TextView) findViewById(R.id.pleaseWait);


        Intent intent = getIntent();
        nearbyPlace = intent.getStringExtra("place");
        nearbyPlace = nearbyPlace.replace(" ", "+");

        image = intent.getIntExtra("image", 0);
        imageView.setImageResource(image);

        Title = intent.getStringExtra("Title");
        //        textView.setText(Title);

        specificMenuList = (RecyclerView) findViewById(R.id.specific_menu_list);
        specificMenuList.setLayoutManager(new LinearLayoutManager(this));
        typeModelAdapter = new TypeModelAdapter(typeModelArrayList, InsideSpecificMenuActivity.this);
        typeModelAdapter.setImageResource(image);
        specificMenuList.setAdapter(typeModelAdapter);


//        placeModelAdapter = new PlaceModelAdapter(placeModelArrayList, InsideSpecificMenuActivity.this);
//        PlaceModel restaurant = new PlaceModel();
//        restaurant.setName("Purple Haze");
//        restaurant.setLocation("Kathmandu");
//        restaurant.setImageUrl("");
//        placeList.add(restaurant);


        new getTypeFromDatabase().execute();

        toolbar = (Toolbar) findViewById(R.id.toolbar3);
        toolbar.setTitle(Title);
        toolbar.setTitleTextColor(getResources().getColor(R.color.White));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        lm = (LocationManager) getSystemService(InsideSpecificMenuActivity.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                longitude = location.getLongitude();
                latitude = location.getLatitude();
                Log.e("latitude", String.valueOf(latitude));
                Log.e("longitude", String.valueOf(longitude));
                String url = getUrl(latitude, longitude, nearbyPlace);
                new getNearby().execute(url, "hahaha");
                Log.e("url", url);


            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Toast.makeText(InsideSpecificMenuActivity.this, "Please, Turn your gps on...", Toast.LENGTH_SHORT).show();
            }
        };
//eanble this
//        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, locationListener);
    }

    public class getTypeFromDatabase extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            URL url;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(ServerConnection.myserver1 + "detail.php"); //Phone ko lagi
//                url = new URL("http://10.0.2.2/app_api/detail.php"); //emulator ko lagi local host connect
//            url = new URL("http://www.google.com"); //emulator ko lagi local host connect

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");

                Log.e("current", "nepal");
                String postParamaters = "type=" + Title.toLowerCase();
                try {
                    DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
                    wr.write(postParamaters.getBytes());
                } catch (Exception e) {


                    Log.e("current1", e.getMessage() + " ");


                }

                InputStream in;
                if (urlConnection.getInputStream() != null) {
                    in = urlConnection.getInputStream();
                } else {
                    in = urlConnection.getErrorStream();
                }

                InputStreamReader isw = new InputStreamReader(in);

                String result = "";
                int data = isw.read();
                while (data != -1) {
                    char current = (char) data;
                    result = result + current;
                    data = isw.read();
                }

                return result;
            } catch (MalformedURLException e) {
                Log.e("Error2", "askdkjs");
            } catch (IOException e) {
                Log.e("Error1", "askdkjs");

            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return "";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            pleaseWait.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                JSONArray jsonArray = new JSONArray(s);

                if (jsonArray.length() == 0) {
                    Toast.makeText(InsideSpecificMenuActivity.this, "No items now", Toast.LENGTH_SHORT).show();
                    return;
                }
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject eachObject = jsonArray.getJSONObject(i);

                    TypeModel typeModel = new TypeModel();
                    typeModel.setId(eachObject.getString("Id"));
                    typeModel.setName(eachObject.getString("Name"));
                    typeModel.setRating(eachObject.getString("Rating"));
                    typeModel.setSpeciality(eachObject.getString("Speciality"));
                    typeModel.setType(eachObject.getString("Type"));
                    typeModel.setCategory(eachObject.getString("Category"));
                    typeModel.setCuisines(eachObject.getString("Cuisines"));
                    typeModel.setEnvironment(eachObject.getString("Environment"));
                    typeModel.setLatitude(eachObject.getString("Latitude"));
                    typeModel.setLongitude(eachObject.getString("Longitude"));
                    typeModel.setLocation(eachObject.getString("Location"));
                    typeModel.setPhNo(eachObject.getString("Number"));
                    Log.e("phone", typeModel.getPhNo() + "");

                    typeModelArrayList.add(typeModel);
                }
                typeModelAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
                pleaseWait.setVisibility(View.GONE);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            Log.e("result", s);
        }
    }


    //yo nearby ko lai list ma dekhauna ko lagi ho but ahile execute garako xaina
    public class getNearby extends AsyncTask<String, String, String> {
        String googlePlacesData;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            pleaseWait.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                Log.d("GetNearbyPlacesData", "doInBackground entered");
                String url = params[0];
                DownloadUrl downloadUrl = new DownloadUrl();
                googlePlacesData = downloadUrl.readUrl(url);
                Log.d("GooglePlacesReadTask", "doInBackground Exit");
            } catch (Exception e) {
                Log.d("GooglePlacesReadTask", e.toString());
            }
            return googlePlacesData;
        }


        protected void onPostExecute(String result) {
            Log.d("GooglePlacesReadTask", "onPostExecute Entered");
            try {
                JSONObject res = new JSONObject(result);
                JSONArray response = res.getJSONArray("results");// Convert response string in to json object.

                placeModelArrayList.clear();
                for (int i = 0; i < response.length(); i++) {
                    JSONObject eachObj = response.getJSONObject(i);
                    JSONObject geo = eachObj.getJSONObject("geometry");
                    JSONObject loc = geo.getJSONObject("location");

                    String latitude = loc.getString("lat");
                    String longitude = loc.getString("lng");
                    String naam = eachObj.getString("name");
                    String vici = eachObj.getString("vicinity");

                    PlaceModel placeModel = new PlaceModel();
                    placeModel.setLatitude(latitude);
                    placeModel.setLongitude(longitude);
                    placeModel.setName(naam);
                    placeModel.setVicinty(vici);

                    placeModelArrayList.add(placeModel);
                }

                placeModelAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
                pleaseWait.setVisibility(View.GONE);
//                specificMenuList.setVisibility(View.VISIBLE);
                Log.e("size", String.valueOf(placeModelArrayList.size()));
                Log.e("json", result);

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("Exception", e.getMessage());
            }

            Log.d("GooglePlacesReadTask", "onPostExecute Exit");
        }
    }

    private void ShowNearbyPlaces(List<HashMap<String, String>> nearbyPlacesList) {
        for (int i = 0; i < nearbyPlacesList.size(); i++) {
            Log.d("onPostExecute", "Entered into Listing locations");
            HashMap<String, String> googlePlace = nearbyPlacesList.get(i);
            double lat = Double.parseDouble(googlePlace.get("lat"));
            double lng = Double.parseDouble(googlePlace.get("lng"));
            String placeName = googlePlace.get("place_name");
        }
    }

    private String getUrl(double latitude, double longitude, String nearbyPlace) {

        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&name=" + nearbyPlace);
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + getApplicationContext().getString(R.string.google_maps_key));
        Log.d("getUrl", googlePlacesUrl.toString());
        return (googlePlacesUrl.toString());
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(InsideSpecificMenuActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(InsideSpecificMenuActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(InsideSpecificMenuActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }
}



