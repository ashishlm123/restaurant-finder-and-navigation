package com.example.ashish.restaurantfinderandnavigation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ashish.restaurantfinderandnavigation.R;
import com.example.ashish.restaurantfinderandnavigation.helper.PlaceModel;

import java.util.ArrayList;

/**
 * Created by Ashish on 03/05/2017.
 */

public class PlaceModelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<PlaceModel> placeModelArrayList;
    private Context context;


    public PlaceModelAdapter(ArrayList<PlaceModel> placeModelArrayList, Context Context) {
        this.placeModelArrayList = placeModelArrayList;
        this.context=Context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PlaceModelAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.inside_specific_menu_list ,parent,false));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PlaceModelAdapter.ViewHolder viewHolder=(PlaceModelAdapter.ViewHolder)holder;
        PlaceModel placeModel=placeModelArrayList.get(position);
        viewHolder.textView1.setText(placeModel.getName());
        viewHolder.textView2.setText(placeModel.getVicinty());
    }

    @Override
    public int getItemCount() {
        Log.e("arraysize", String.valueOf(placeModelArrayList.size()));
        return placeModelArrayList.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{
        TextView textView1, textView2;

        public ViewHolder(View itemView) {
            super(itemView);
            textView1= (TextView)itemView.findViewById(R.id.nearby_place_title);
            textView2 = (TextView) itemView.findViewById(R.id.nearby_place_location);


        }}
}


