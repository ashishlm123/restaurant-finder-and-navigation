package com.example.ashish.restaurantfinderandnavigation.helper;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import java.util.Map;
import java.util.Set;

/**
 * Created by User on 2016-03-04.
 */
public class PreferenceHelper implements SharedPreferences {
    public static final String APP_SHARED_PREFS = "restaurant_prefs";

    public static final String GCM_TOKEN = "gcmToken";
    public static final String IS_LOGIN = "is_login";

    //tokens
    public static final String TOKEN = "app_user_api_key";
    //user detail
    public static final String APP_USER_ID = "app_user_id";
    public static final String APP_USER_EMAIL = "app_user_email";
    public static final String APP_USER_NAME = "app_user_name";
    public static final String APP_USER_LOGIN_NAME = "app_user_full_name";
    public static final String FB_APP_ID = "fb_app_id";

    private SharedPreferences prefs;
    private static PreferenceHelper helper;

    public static PreferenceHelper getInstance(Context context) {
        if (helper != null) {
            return helper;
        } else {
            helper = new PreferenceHelper(context);
            return helper;
        }

    }

    private PreferenceHelper(Context context) {
        prefs = context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
    }

    public SharedPreferences getPrefs() {
        return prefs;
    }

    @Override
    public Map<String, ?> getAll() {
        return prefs.getAll();
    }

    @Override
    public String getString(String s, String s2) {
        return prefs.getString(s, s2);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public Set<String> getStringSet(String s, Set<String> strings) {
        return prefs.getStringSet(s, strings);
    }

    @Override
    public int getInt(String s, int i) {
        return prefs.getInt(s, i);
    }

    @Override
    public long getLong(String s, long l) {
        return prefs.getLong(s, l);
    }

    @Override
    public float getFloat(String s, float v) {
        return prefs.getFloat(s, v);
    }

    @Override
    public boolean getBoolean(String s, boolean b) {
        return prefs.getBoolean(s, b);
    }

    @Override
    public boolean contains(String s) {
        return prefs.contains(s);
    }

    @Override
    public Editor edit() {
        return prefs.edit();
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {

    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {

    }

    public boolean isLogin() {
        return prefs.getBoolean(IS_LOGIN, false);
    }

    public String getUserId() {
        return prefs.getString(APP_USER_ID, "-1");
    }

    public String getUserEmail() {
        return prefs.getString(APP_USER_EMAIL, "");
    }

    public String getToken() {
        return prefs.getString(TOKEN, "k14eAjXdnUNNWOovQFnTtqtMrwos2Xe4");
    }

    public String getUserName() {
        return prefs.getString(APP_USER_NAME, "");
    }

}
