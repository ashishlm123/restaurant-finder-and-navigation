package com.example.ashish.restaurantfinderandnavigation;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class MainActivity extends AppCompatActivity {
    SharedPreferences pref;
    public final static String EXTRA_MESSAGE = "MESSAGE";

    private final int Splash_display_length = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pref = getSharedPreferences("log", Context.MODE_PRIVATE);

        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.example.ashish.restaurantfinderandnavigation",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String sign = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("MY KEY HASH:", sign);
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String USERNAME = pref.getString("Username:- ", "");
                if (TextUtils.isEmpty(USERNAME)) {
                    Intent mainIntent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(mainIntent);
                } else {
                    Intent mainIntent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(mainIntent);
                }
                MainActivity.this.finish();
            }

        }, Splash_display_length);


    }

}
