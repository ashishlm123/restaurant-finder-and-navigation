package com.example.ashish.restaurantfinderandnavigation.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.ashish.restaurantfinderandnavigation.LoginActivity;
import com.example.ashish.restaurantfinderandnavigation.R;

import org.w3c.dom.Text;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Ashish on 24/03/2017.
 */

public class MyAccount extends Fragment {

    FragmentManager fm;
    SharedPreferences pref;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_account, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pref = getActivity().getSharedPreferences("log", Context.MODE_PRIVATE);

        //Initialization to take profile pic and profile name from connected accounts.
        ImageView profile_pic = (ImageView)view.findViewById(R.id.profile_pic);
        Log.e("image",pref.getString("profile_url","https://graph.facebook.com"));
        Glide.with(this).load(pref.getString("profile_url","https://graph.facebook.com"))
                .into(profile_pic);

        Intent getname = getActivity().getIntent();
        String name = getname.getStringExtra("username");
        TextView profile_name = (TextView)view.findViewById(R.id.profile_name);
        profile_name.setText(name);

        //Profile ko lagi
        LinearLayout edit_profile = (LinearLayout) view.findViewById(R.id.edit_profile);
        final TextView edit_profile_text = (TextView) edit_profile.findViewById(R.id.edit_us);
        edit_profile_text.setText("Edit Profile");
        ImageView edit_profile_icon = (ImageView) edit_profile.findViewById(R.id.edit_us_img);
        edit_profile_icon.setImageResource(R.drawable.ic_edit_black_24dp);
        edit_profile_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Edit Profile", Toast.LENGTH_SHORT).show();
            }
        });


        //Others ko lagi

        LinearLayout invite_friends = (LinearLayout) view.findViewById(R.id.invite_friends_to_lingo_finder);
        TextView invite_friends_text = (TextView) invite_friends.findViewById(R.id.edit_us);
        invite_friends_text.setText("Invite Friends");
        ImageView invite_friends_icon = (ImageView) invite_friends.findViewById(R.id.edit_us_img);
        invite_friends_icon.setImageResource(R.drawable.invite_friends);

        LinearLayout add_place = (LinearLayout) view.findViewById(R.id.add_a_place);
        TextView add_place_text = (TextView) add_place.findViewById(R.id.edit_us);
        add_place_text.setText("Add a place.");
        ImageView add_place_icon = (ImageView) add_place.findViewById(R.id.edit_us_img);
        add_place_icon.setImageResource(R.drawable.add_place);


        LinearLayout rate_us = (LinearLayout) view.findViewById(R.id.rate_us);
        TextView rate_us_text = (TextView) rate_us.findViewById(R.id.edit_us);
        rate_us_text.setText("Rate Us");
        ImageView rate_us_icon = (ImageView) rate_us.findViewById(R.id.edit_us_img);
        rate_us_icon.setImageResource(R.drawable.rate_us);


        LinearLayout sign_out = (LinearLayout) view.findViewById(R.id.log_out);
        TextView sign_out_text = (TextView) sign_out.findViewById(R.id.edit_us);
        sign_out_text.setText("Log Out");
        ImageView sign_out_icon = (ImageView) sign_out.findViewById(R.id.edit_us_img);
        sign_out_icon.setImageResource(R.drawable.log_out);
        sign_out_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pref.edit().clear().commit();
                Intent intent_login = new Intent(getApplicationContext(),LoginActivity.class);
                intent_login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent_login);
            }
        });


    }
}
