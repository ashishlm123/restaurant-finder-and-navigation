package com.example.ashish.restaurantfinderandnavigation;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.ashish.restaurantfinderandnavigation.adapter.PlaceModelAdapter;
import com.example.ashish.restaurantfinderandnavigation.adapter.TypeModelAdapter;
import com.example.ashish.restaurantfinderandnavigation.helper.PlaceModel;
import com.example.ashish.restaurantfinderandnavigation.helper.TypeModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Ashish on 19/05/2017.
 */

public class RecommendActivity extends AppCompatActivity {

    ArrayList<PlaceModel> placeModelArrayList;
    PlaceModelAdapter placeModelAdapter;
    ArrayList<TypeModel> typeModelArrayList;
    ProgressBar progressBar1;
    RecyclerView recommendationList;
    TextView pleaseWait1;
    TypeModelAdapter typeModelAdapter;
    TextView errorText, resultQuantity;
    FusedLocationProviderClient mFusedLocationClient;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    Double lat, lng;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitvity_recommend);

        typeModelArrayList = new ArrayList<>();
        placeModelArrayList = new ArrayList<>();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Recommend Me");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);
        progressBar1.getIndeterminateDrawable().setColorFilter(0xFFFF0000, android.graphics.PorterDuff.Mode.MULTIPLY);
        pleaseWait1 = (TextView) findViewById(R.id.pleaseWait1);
        errorText = (TextView) findViewById(R.id.error_text);
        resultQuantity = (TextView) findViewById(R.id.resultQty);

        recommendationList = (RecyclerView) findViewById(R.id.recommendationList);
        recommendationList.setLayoutManager(new LinearLayoutManager(this));
        placeModelAdapter = new PlaceModelAdapter(placeModelArrayList, RecommendActivity.this);
        typeModelAdapter = new TypeModelAdapter(typeModelArrayList, RecommendActivity.this);
        recommendationList.setAdapter(typeModelAdapter);

        Intent intent = getIntent();
        final String Cuisines = intent.getStringExtra("Cuisines");
        final String Type = intent.getStringExtra("Type");
        final String Category = intent.getStringExtra("Category");
        final String Speciality = intent.getStringExtra("Speciality");
        final String Environment = intent.getStringExtra("Environment");

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(RecommendActivity.this);
        mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    Log.e("hello", "I am inside if statement");
                    lat = location.getLatitude();
                    lng = location.getLongitude();
                    Log.e("latitude" + " " + lat, "longitude" + " " + lng);
                    new getTypeFromDatabase().execute(Cuisines, Type, Category, Speciality, Environment);
                } else {
                    Log.e("hello", "I am inside else statement");
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        checkLocationPermission();
                    }
                    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(RecommendActivity.this);
                    mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            lat = location.getLatitude();
                            lng = location.getLongitude();
                            Log.e("latitude" + " " + lat, "longitude" + " " + lng);
                            new getTypeFromDatabase().execute(Cuisines, Type, Category, Speciality, Environment);
                        }
                    });

                }
            }
        });


    }

    public class getTypeFromDatabase extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {

            String Cuisines = strings[0];
            String Type = strings[1];
            String Category = strings[2];
            String Speciality = strings[3];
            String Environment = strings[4];

            URL url;
            HttpURLConnection urlConnection = null;

            try {


//                url = new URL(ServerConnection.myserver1 + "algorithmdetail.php");
                url = new URL(ServerConnection.myserver1 + "algorithmdetail.php");
                Log.e("calledUrl", url.toString());

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");

                Log.e("current", "nepal");
                //Sending postparameters to API
//                String postParamaters = "Cuisines=" + URLEncoder.encode(Cuisines) + "&type=" + URLEncoder.encode(Type)
//                        + "&Category=" + URLEncoder.encode(Category) + "&Speciality=" + URLEncoder.encode(Speciality)
//                        + "&Environment=" + URLEncoder.encode(Environment);

                String postParamaters = "Cuisines=" + URLEncoder.encode(Cuisines) + "&type=" + URLEncoder.encode(Type)
                        + "&Category=" + URLEncoder.encode(Category) + "&Speciality=" + URLEncoder.encode(Speciality)
                        + "&Environment=" + URLEncoder.encode(Environment) + "&Lat=" + lat + "&Lng=" + lng;
                Log.e("PostParameter", postParamaters);
                try {
                    DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
                    wr.write(postParamaters.getBytes());
                } catch (Exception e) {
                    Log.e("current1", e.getMessage() + " ");
                }

                InputStream in;
                if (urlConnection.getInputStream() != null) {
                    in = urlConnection.getInputStream();
                } else {
                    in = urlConnection.getErrorStream();
                }

                InputStreamReader isw = new InputStreamReader(in);

                String result = "";
                int data = isw.read();
                while (data != -1) {
                    char current = (char) data;
                    result = result + current;
                    data = isw.read();
                }

                return result;
            } catch (MalformedURLException e) {
                Log.e("Error2", "askdkjs");
            } catch (IOException e) {
                Log.e("Error1", "askdkjs");
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return "";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar1.setVisibility(View.VISIBLE);
            pleaseWait1.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                JSONArray jsonArray = new JSONArray(s);


                Log.e("length", String.valueOf(jsonArray.length()));
                resultQuantity.setText("(" + String.valueOf(jsonArray.length()) + " " + "Results" + ")");
                if (jsonArray.length() == 0) {
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText("No results in the database..");
                } else {

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject eachObject = jsonArray.getJSONObject(i);

                        TypeModel typeModel = new TypeModel();
                        typeModel.setId(eachObject.getString("Id"));
                        typeModel.setName(eachObject.getString("Name"));
                        typeModel.setRating(eachObject.getString("Rating"));
                        typeModel.setSpeciality(eachObject.getString("Speciality"));
                        typeModel.setType(eachObject.getString("Type"));
                        typeModel.setCategory(eachObject.getString("Category"));
                        typeModel.setCuisines(eachObject.getString("Cuisines"));
                        typeModel.setEnvironment(eachObject.getString("Environment"));
                        typeModel.setLatitude(eachObject.getString("Latitude"));
                        typeModel.setLongitude(eachObject.getString("Longitude"));
                        typeModel.setLocation(eachObject.getString("Location"));
                        typeModel.setPhNo(eachObject.getString("Number"));

                        typeModelArrayList.add(typeModel);
                    }

                    typeModelAdapter.notifyDataSetChanged();
                    progressBar1.setVisibility(View.GONE);
                    pleaseWait1.setVisibility(View.GONE);
                }

            } catch (JSONException e) {
                errorText.setVisibility(View.VISIBLE);
                errorText.setText("No results in the database..");
                resultQuantity.setText("No Results");
                progressBar1.setVisibility(View.GONE);
                pleaseWait1.setVisibility(View.GONE);
            }


            Log.e("result", s);
        }
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }
}
