package com.example.ashish.restaurantfinderandnavigation;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ashish.restaurantfinderandnavigation.fragments.MyHome;
import com.example.ashish.restaurantfinderandnavigation.fragments.MyLocation;
import com.example.ashish.restaurantfinderandnavigation.helper.PreferenceHelper;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

/**
 * Created by Ashish on 24/03/2017.
 */

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    NavigationView navigationView;
    private DrawerLayout mDrawer;
    Toolbar toolbar;
    String type;
    FragmentManager fm;
    SharedPreferences pref;
    PreferenceHelper helper;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        pref = getSharedPreferences("log", Context.MODE_PRIVATE);
        helper = PreferenceHelper.getInstance(this);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View nav_header_view = navigationView.getHeaderView(0);
        String name = pref.getString("username1", "");

        TextView profile_name = nav_header_view.findViewById(R.id.profile_name);
        profile_name.setText(name);
//        profile_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                fm.beginTransaction().replace(R.id.id_container_menu, new MyAccount()).commit();
//                mDrawer.closeDrawer(GravityCompat.START);
//            }
//        });


        fm = getSupportFragmentManager();

        toolbar = (Toolbar) findViewById(R.id.toolbarnav);
        toolbar.setTitle("Home");

        FloatingActionMenu fam = (FloatingActionMenu) findViewById(R.id.material_design_android_floating_action_menu);
        FloatingActionButton myLocation = (FloatingActionButton) findViewById(R.id.material_design_floating_action_menu_item1);


        myLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(HomeActivity.this, "Your Current Location", Toast.LENGTH_SHORT).show();
                fm.beginTransaction().replace(R.id.id_container_menu, new MyLocation()).commit();
                toolbar.setTitle("My Location");
            }
        });

        mDrawer = (DrawerLayout) findViewById(R.id.drawerlayout);
        setupMenu();

        fm.beginTransaction().replace(R.id.id_container_menu, new MyHome()).commit();
    }


    private void setupMenu() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(HomeActivity.this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                fm.beginTransaction().replace(R.id.id_container_menu, new MyHome()).commit();
                toolbar.setTitle("Home");
//                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
//                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
//                startActivity(intent);
                break;

            case R.id.nav_location:
                if (checkInternetConnection()) {
                    fm.beginTransaction().replace(R.id.id_container_menu, new MyLocation()).commit();
                    toolbar.setTitle("My Location");
                } else {
                    Toast.makeText(this, "Please, Check you internet connection...", Toast.LENGTH_SHORT).show();
                    fm.beginTransaction().replace(R.id.id_container_menu, new MyLocation()).commit();
                }
                break;

            case R.id.nav_sign_out:
                pref.edit().clear().commit();
                Intent intent_login = new Intent(getApplicationContext(), LoginActivity.class);
                intent_login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent_login);
                break;

            case R.id.nav_recommend_me:
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(HomeActivity.this);
                View view1 = LayoutInflater.from(HomeActivity.this).inflate(R.layout.cutsom_dialog_recommend, null, false);
                alertDialogBuilder.setView(view1);
                final Spinner cuisineSpinner = view1.findViewById(R.id.cuisineSpinner);
                final Spinner typeSpinner = view1.findViewById(R.id.typeSpinner);
                final Spinner categorySpinner = view1.findViewById(R.id.categorySpinner);
                final Spinner specialitySpinner = view1.findViewById(R.id.specialitySpinner);
                final Spinner environmentSpinner = view1.findViewById(R.id.environmentSpinner);
                alertDialogBuilder.setPositiveButton("Go", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Intent intent_recommend = new Intent(getApplicationContext(), RecommendActivity.class);
                        if (cuisineSpinner.getSelectedItemPosition() == 0) {
                            intent_recommend.putExtra("Cuisines", "");

                        } else {
                            intent_recommend.putExtra("Cuisines", (String) cuisineSpinner.getSelectedItem());
                        }

                        if (typeSpinner.getSelectedItemPosition() == 0) {
                            intent_recommend.putExtra("Type", "");
                        } else {
                            intent_recommend.putExtra("Type", (String) typeSpinner.getSelectedItem());
                        }

                        if (categorySpinner.getSelectedItemPosition() == 0) {
                            intent_recommend.putExtra("Category", "");
                        } else {
                            intent_recommend.putExtra("Category", (String) categorySpinner.getSelectedItem());
                        }

                        if (specialitySpinner.getSelectedItemPosition() == 0) {
                            intent_recommend.putExtra("Speciality", "");
                        } else {
                            intent_recommend.putExtra("Speciality", (String) specialitySpinner.getSelectedItem());
                        }

                        if (environmentSpinner.getSelectedItemPosition() == 0) {
                            intent_recommend.putExtra("Environment", "");
                        } else {
                            intent_recommend.putExtra("Environment", (String) environmentSpinner.getSelectedItem());
                        }

                        startActivity(intent_recommend);
                    }
                });
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                alertDialogBuilder.show();
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean checkInternetConnection() {
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        // ARE WE CONNECTED TO THE NET
        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected()) {
            return true;
        }
        return false;
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }
}
