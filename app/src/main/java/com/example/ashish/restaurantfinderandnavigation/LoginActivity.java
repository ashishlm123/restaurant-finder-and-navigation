package com.example.ashish.restaurantfinderandnavigation;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.ashish.restaurantfinderandnavigation.helper.PreferenceHelper;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ashish on 23/03/2017.
 */

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    CallbackManager callbackManager;
    SharedPreferences pref;
    private GoogleApiClient mGoogleApiClient;
    private static final int GOOGLE_SIGN_IN = 121;
    ImageView profile_pic;
    EditText password, userName;

    PreferenceHelper helper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(LoginActivity.this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);

        helper = PreferenceHelper.getInstance(this);

        //For shared preferences

        password = (EditText) findViewById(R.id.username);
        userName = (EditText) findViewById(R.id.password);

        pref = getSharedPreferences("log", Context.MODE_PRIVATE);


        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();


        //For Creating Account.
        TextView create_account = (TextView) findViewById(R.id.create_account);
        create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_signUp = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(intent_signUp);
            }
        });

        //For Log In.
        Button log_in = (Button) findViewById(R.id.login_button);
        log_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginTask(userName.getText().toString().trim(), password.getText().toString().trim());
//                final String USERNAME = pref.getString("Username:- ", "");
//                final String PASSWORD = pref.getString("Password:- ", "");
//
//                if (pref.contains("Username:- ")) {
//
//
//                    if (USERNAME.equals(enter_username.getText().toString().trim())
//                            && PASSWORD.equals(enter_password.getText().toString().trim())) {
//
//                        pref.edit().clear().commit();
//                        Intent intent_home = new Intent(getApplicationContext(),HomeActivity.class);
//                        intent_home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent_home);
//
//                    } else {
//                        Toast.makeText(LoginActivity.this, "Not Registered Yet!! ", Toast.LENGTH_SHORT).show();
//
//
//                    }
//                } else {
//                    Toast.makeText(LoginActivity.this, "Not Registered Yet!! ", Toast.LENGTH_SHORT).show();
//                    pref.edit().clear().commit();
//                    Intent intent_home = new Intent(getApplicationContext(),HomeActivity.class);
//                    intent_home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent_home);
//                }
            }
        });


        //For Facebook Login

        TextView facebook_button = (TextView) findViewById(R.id.facebook_button);
        facebook_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInternetConnection()) {
                    fbSignIn();
                    Log.e("hello", "you invoked fbSignIn() ");

                } else {
                    Toast.makeText(LoginActivity.this, "Please, check your internet connection...", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //For Google Login

        TextView google_button = (TextView) findViewById(R.id.google_button);
        google_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInternetConnection()) {
                    googleSignIn();
                    Log.e("hello", "you invoked googleSignIn");
                } else {
                    Toast.makeText(LoginActivity.this, "Please, check your internet connection...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loginTask(final String password, final String username) {
        String url = ServerConnection.myserver1 + "login.php";
        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setIcon(R.drawable.temp_app_logo);
        progressDialog.show();

        StringRequest req = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("status")) {
                        if (jsonObject.getString("status").equals("true")) {
                            String username = jsonObject.getString("username");
                            String email = jsonObject.getString("email");
                            Intent intent_home = new Intent(getApplicationContext(), HomeActivity.class);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("username1", username);
                            editor.commit();
                            intent_home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent_home);
                        }
                    } else {
                        Log.e("Message2", jsonObject.getString("message") + "   ");

                        Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    Log.e("dn", e.getMessage());
                }

                Log.e("Message", "jjjjj   ");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    JSONArray errors = new JSONArray(error);
                } catch (JSONException e) {

                    Log.e("mesg", error.getMessage() + "");
                    e.printStackTrace();
                }
                Log.e("mesgww", error.getMessage() + "");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> postparameter = new HashMap<>();
                postparameter.put("username", username);
                postparameter.put("password", password);
                Log.e("hellow", username + "   " + password);
                return postparameter;

            }
        };

        Volley.newRequestQueue(getApplicationContext()).add(req);


    }


    private void fbSignIn() {
        Log.e("hello1", "You are here too");
        callbackManager = CallbackManager.Factory.create();
//        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this,
                (Arrays.asList("public_profile", "user_birthday", "user_location", "user_friends", "email")));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.e("fbTask onSuccess", loginResult.getAccessToken() + " ** ");
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        Log.e("fbTask res", "" + object);
                                        // Application code
                                        try {
                                            Log.e("ashish", "nepal");
                                            String id = object.getString("id");
                                            String first_name = object.getString("first_name");
                                            String last_name = object.getString("last_name");
                                            String email = object.optString("email"); //Get null value here

                                            pref.edit().clear().commit();
                                            Intent intent_facebook = new Intent(getApplicationContext(), HomeActivity.class);
                                            intent_facebook.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                            SharedPreferences.Editor editor = pref.edit();
                                            editor.putString("Username:- ", first_name + " " + last_name);
                                            editor.putString("fb_id", id);
                                            editor.putString("fb_email", email);
                                            editor.putString("profile_url", "https://graph.facebook.com/" + id + "/picture?type=large");
                                            editor.putString("username1", first_name + " " + last_name);
                                            editor.commit();


                                            LoginManager.getInstance().logOut();
                                            startActivity(intent_facebook);

                                        } catch (JSONException e) {
                                            Log.e("fbTask error", e.getMessage() + "");
                                            LoginManager.getInstance().logOut();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name, first_name, last_name, email, gender");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.e("fbTask onCancel", "onCancel()");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.e("fbTask onError", exception.getMessage() + " fk");
                    }
                });
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode, resultCode, data);
//    }

    private void googleSignIn() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(intent, GOOGLE_SIGN_IN);
    }

    //google api
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("Google sign-in Failed: ", connectionResult.getErrorMessage() + "");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            pref.edit().clear().commit();
            Intent intent_google = new Intent(getApplicationContext(), HomeActivity.class);
            intent_google.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


            SharedPreferences.Editor editor = pref.edit();
            editor.putString("Username:- ", acct.getGivenName() + " " + acct.getFamilyName());
            if (acct.getPhotoUrl() != null) {
                editor.putString("profile_url", acct.getPhotoUrl().toString());
            }
            editor.putString("google_id", acct.getId());
            editor.putString("google_email", acct.getEmail());
            editor.putString("google_name", acct.getDisplayName());
            String disPlayName = acct.getDisplayName();

            editor.putString("username1", disPlayName);
            editor.commit();

//            helper.edit().putString(PreferenceHelper.APP_USER_NAME, disPlayName);


            signOut();
            startActivity(intent_google);
            Log.e("google login success", "Display Name = " + acct.getDisplayName() + "\nEmail = "
                    + acct.getEmail() + "\nGivenName = " + acct.getGivenName() + "\nFamilyName = " +
                    acct.getFamilyName() + "\nId = " + acct.getId() + "\nPhotoUrl = " + acct.getPhotoUrl());


        } else {
            // Signed out, show unauthenticated UI.
            Log.e("google isSuccess", "false");
        }
    }

    // [START signOut]
    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        // [START_EXCLUDE]
                        Log.e("google signOut", status.getStatus().getStatusMessage() + " ** "
                                + status.isSuccess());
                        // [END_EXCLUDE]
                    }
                });
    }

    private boolean checkInternetConnection() {
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        // ARE WE CONNECTED TO THE NET
        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected()) {
            return true;
        }
        return false;
    }
}







