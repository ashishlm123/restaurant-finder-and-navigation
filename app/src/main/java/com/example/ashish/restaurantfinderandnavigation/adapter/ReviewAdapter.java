package com.example.ashish.restaurantfinderandnavigation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ashish.restaurantfinderandnavigation.R;
import com.example.ashish.restaurantfinderandnavigation.helper.ReviewModel;

import java.util.ArrayList;


/**
 * Created by Ashish on 01/06/2017.
 */

public class ReviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<ReviewModel> reviewArrayList;

    public ReviewAdapter(ArrayList<ReviewModel> reviewList, Context context) {
        this.reviewArrayList = reviewList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ReviewAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_review_list, parent, false));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//        ReviewAdapter.ViewHolder viewHolder=(ReviewAdapter.ViewHolder)holder;
//        ReviewModel reviewModel=reviewArrayList.get(position);
    }

    @Override
    public int getItemCount() {
//        return reviewArrayList.size();
        return 10;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {


        ViewHolder(View itemView) {
            super(itemView);



        }
    }
}
